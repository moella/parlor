import 'package:cards/pager.dart';
import 'package:cards/pages/auth/login.dart';
import 'package:cards/services/auth_service.dart';
import 'package:cards/services/game_service.dart';
import 'package:cards/services/user_service.dart';
import 'package:cards/theme.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(
      MultiProvider(
          providers: [
            ChangeNotifierProvider<AuthService>.value(value: AuthService()),
            ChangeNotifierProvider<UserService>.value(value: UserService()),
            ChangeNotifierProvider<GameService>.value(value: GameService()),
          ],
          child: const App())
  );
}

class App extends StatelessWidget {
  const App({super.key});

  static const Color primaryColor = Colors.blue;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Parlor',

        theme: AppTheme.getCurrentTheme(),

        localizationsDelegates: const [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],

        supportedLocales: const [
          Locale('en'),
          Locale('de')
        ],
        debugShowCheckedModeBanner: false,
        home: Consumer<AuthService>(
            builder: (context, authService, child) => FirebaseAuth.instance.currentUser != null
                ? const Pager()
                : const Login())
    );
  }
}
