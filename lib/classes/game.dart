import 'package:cloud_firestore/cloud_firestore.dart';

interface class Game {
  String name;
  String description;
  String creator;
  DateTime creationTime;

  List<Variation> variations;
  int startValue;
  int endValue;

  Game({
    required this.name,
    required this.description,
    required this.creator,
    required this.creationTime,
    required this.variations,
    required this.startValue,
    required this.endValue,
  });

  static Game fromDoc(QueryDocumentSnapshot<Map<String, dynamic>> doc, List<Variation> variations) {
    return Game(
      name: doc['name'],
      description: doc['description'],
      creator: doc['creator'],
      creationTime: doc['creation_time'].toDate(),
      variations: variations,
      startValue: doc['start_value'],
      endValue: doc['end_value'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'description': description,
      'creator': creator,
      'creation_time': creationTime,
      'start_value': startValue,
      'end_value': endValue,
    };
  }

  @override
  String toString() {
    return 'Game{name: $name}';
  }
}

interface class Variation {
  String name;
  String description;
  bool defaultValue;

  Variation({
    required this.name,
    required this.description,
    required this.defaultValue,
  });

  static Variation fromDynamic(dynamic doc) {
    return Variation(
      name: doc['name'],
      description: doc['description'],
      defaultValue: doc['defaultValue'],
    );
  }

  toJson() {
    return {
      'name': name,
      'description': description,
      'defaultValue': defaultValue,
    };
  }
}
