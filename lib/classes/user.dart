class UserDetails {
  String username;

  UserDetails(this.username);

  Map<String, dynamic> toMap() {
    return {
      'username': username,
    };
  }

  factory UserDetails.fromJson(Map<String, dynamic> json) {
    return UserDetails(
      json['username'],
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is UserDetails &&
              runtimeType == other.runtimeType &&
              username == other.username;

  @override
  int get hashCode => username.hashCode;
}
