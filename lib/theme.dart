import 'package:flutter/material.dart';

class AppTheme {
  static const MaterialColor _primaryColor = Colors.deepPurple;
  static const Color _accentColor = Colors.purpleAccent;

  static ThemeData getCurrentTheme() {
    return _getLightTheme();
  }

  static ThemeData _getLightTheme() {
    return ThemeData(
      primaryColor: _primaryColor,
      primarySwatch: Colors.purple,

      appBarTheme: const AppBarTheme(
        backgroundColor: _primaryColor,
        foregroundColor: Colors.white,
      ),

      colorScheme: ColorScheme.fromSwatch(
        primarySwatch: _primaryColor,
        accentColor: _accentColor,
        backgroundColor: const Color(0xfff5f5f5)
      ),

      cardColor: const Color(0xff6ea8ff),

      inputDecorationTheme: InputDecorationTheme(
        iconColor: _primaryColor,
        prefixIconColor: _primaryColor,

        fillColor: Colors.white,
        filled: true,

        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.black12),
          borderRadius: BorderRadius.circular(20),
        ),

        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: _primaryColor),
          borderRadius: BorderRadius.circular(20),
        ),
      ),

      bottomAppBarTheme: BottomAppBarTheme(
        color: Color.lerp(_primaryColor, Colors.white, 0.8)
      ),

      radioTheme: RadioThemeData(
        fillColor: MaterialStateProperty.all(Colors.white)
      )
    );
  }
}
