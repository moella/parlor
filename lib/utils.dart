import 'package:flutter/material.dart';

Widget futureBuilderOrLoading<T>(
    {
      required Future<T> future,
      required Widget Function(T?) builder,
      Widget loading = const Center(child: CircularProgressIndicator())
    }) {
  return FutureBuilder(
      future: future, 
      builder: (context, snapshot) {
        if (snapshot.hasData && snapshot.data != null) {
          return builder(snapshot.data);
        }

        return loading;
      });
}
