import 'dart:developer';

import 'package:cards/classes/game.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';


class GameService extends ChangeNotifier {
  List<Game>? _games;

  Future<List<Game>> get games async {
    if (_games == null) {
      await loadGames();
    }

    return _games!;
  }

  Future<void> loadGames() async {
    log("loading games");

    List<Game> loadedGames = [];
    QuerySnapshot<Map<String, dynamic>> ref = await FirebaseFirestore.instance.collection('games').get();

    for (var doc in ref.docs) {
      if (doc.exists) {
        loadedGames.add(Game.fromDoc(doc, []));
      }
    }

    _games = loadedGames;
    notifyListeners();
  }

  Future<void> saveGame(Game game) async {
    var createdRef = await FirebaseFirestore.instance.collection('games').add(game.toJson());

    for (var variation in game.variations) {
      await createdRef.collection('rules').add(variation.toJson());
    }

    if (_games == null) {
      await loadGames();
    }
    
    _games!.add(game);
    notifyListeners();
  }

  static GameService of(BuildContext context) {
    return Provider.of<GameService>(context, listen: false);
  }
}
