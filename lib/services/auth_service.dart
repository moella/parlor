import 'package:cards/services/user_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AuthService with ChangeNotifier {
  Future<String?> register({
    required BuildContext context,
    required String email,
    required String password,
    required String displayName
  }) async {
    UserService userService = UserService.of(context);

    UserCredential userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(email: email, password: password);

    if (userCredential.user != null) {
      await userCredential.user!.updateDisplayName(displayName);
      await userService.updateUsername(displayName);

      return userCredential.user!.uid;
    }

    return null;
  }

  Future<void> login({
        required String email,
        required String password
  }) async {
    await FirebaseAuth.instance.signInWithEmailAndPassword(email: email, password: password);
    notifyListeners();
  }

  Future<void> logout() async {
    await FirebaseAuth.instance.signOut();
    notifyListeners();
  }

  static AuthService of(BuildContext context) {
    return Provider.of<AuthService>(context, listen: false);
  }
}
