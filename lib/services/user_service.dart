import 'dart:developer';

import 'package:cards/classes/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class UserService extends ChangeNotifier {
  UserDetails? _userDetails;

  Future<UserDetails?> get userDetails async {
    if (_userDetails == null) {
      await loadUserDetails();
    }

    return _userDetails;
  }

  Future<void> loadUserDetails() async {
    log("loading user details");

    User? user = FirebaseAuth.instance.currentUser;

    if (user != null) {
      DocumentSnapshot<Map<String, dynamic>> ref = await FirebaseFirestore.instance.collection('users').doc(user.uid).get();

      if (ref.exists) {
        _userDetails = UserDetails(
            ref.get('username')
        );

        notifyListeners();
      }
    }
  }

  Future<void> updateUsername(String username) async {
    await FirebaseAuth.instance.currentUser?.updateDisplayName(username);

    String? uid = FirebaseAuth.instance.currentUser?.uid;

    if (uid != null) {
      await FirebaseFirestore.instance.collection('users').doc(uid).set({
        'username': username
      });

      _userDetails?.username = username;
    }

    notifyListeners();
  }

  Future<void> updateEmail(String email) async {
    User? user = FirebaseAuth.instance.currentUser;

    if (user != null) {
      await user.verifyBeforeUpdateEmail(email);
    }

    notifyListeners();
  }

  String getUsername(BuildContext context) {
    AppLocalizations localizations = AppLocalizations.of(context)!;

    return FirebaseAuth.instance.currentUser?.displayName ?? localizations.userNotCouldNotBeLoaded;
  }

  Future<String?> getUsernameFromUID(String uid) async {
    DocumentSnapshot<Map<String, dynamic>> ref =
        await FirebaseFirestore.instance.collection('users').doc(uid).get();

    if (ref.exists) {
      return ref.get('username');
    }

    return null;
  }

  static UserService of(BuildContext context) {
    return Provider.of<UserService>(context, listen: false);
  }
}
