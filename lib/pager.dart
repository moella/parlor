import 'package:cards/pages/games/games.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'pages/settings/settings.dart';

class Pager extends StatefulWidget {
  const Pager({super.key});

  @override
  State<Pager> createState() => _PagerState();
}

class _PagerState extends State<Pager> {
  ValueNotifier<bool> openCloseDial = ValueNotifier(false);
  int _currentIndex = 0;

  static const List<Widget> pages = [
    Placeholder(),
    Games(),
    Placeholder(),
    Profile(),
  ];

  @override
  Widget build(BuildContext context) {
    AppLocalizations localizations = AppLocalizations.of(context)!;

    return SafeArea(
      child: Scaffold(
        extendBody: true,
        body: IndexedStack(index: _currentIndex, children: pages),

        bottomNavigationBar: BottomAppBar(
          height: 70,
          padding: const EdgeInsets.all(5),
          shape: const CircularNotchedRectangle(),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              _buildNavBarItem(0, Icons.list_alt_sharp, localizations.menu_entries),
              _buildNavBarItem(1, Icons.games, localizations.menu_games),
              _buildNavBarItem(2, Icons.group, localizations.menu_groups),
              _buildNavBarItem(3, Icons.settings, localizations.menu_settings),
            ],
          ),
        ),

        floatingActionButton: _currentIndex == 0
            ? FloatingActionButton(onPressed: () {})
            : null,
      ),
    );
  }

  Widget _buildNavBarItem(int index, IconData icon, [String? text]) {
    Widget iconWidget;

    if (text == null) {
      iconWidget = Icon(icon, color: _currentIndex == index ? Colors.purple : Colors.black);
    } else {
      iconWidget = Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(icon, color: _currentIndex == index ? Colors.purple : Colors.black),
          Text(text),
          const SizedBox(width: 75)
        ],
      );
    }

    return IconButton(
      icon: iconWidget,
      onPressed: () => _switchPage(index),
    );
  }

  void _switchPage(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
