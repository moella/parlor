import 'package:cards/classes/user.dart';
import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';
import 'package:provider/provider.dart';
import '../../services/auth_service.dart';
import '../../services/user_service.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Registration extends StatefulWidget {
  const Registration({super.key, required this.user, required this.onSave, required this.confirmText} );
  final UserDetails? user;
  final void Function(UserDetails) onSave;
  final String confirmText;

  @override
  State<Registration> createState() => _RegistrationState();
}

class _RegistrationState extends State<Registration> {
  TextEditingController username = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  bool valid = false;
  bool emailTouched = false;
  bool passwordTouched = false;
  bool registerIn = false;
  late AppLocalizations localizations;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    localizations = AppLocalizations.of(context)!;

    return Scaffold(
      body: SingleChildScrollView(
        child:  Consumer<UserService>(
        builder: (context, userService, child) {
          return Padding(
            padding: const EdgeInsets.all(15),
            child: Form(
                key: formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    TextFormField(
                      onChanged: (value) => setState(() => valid = isValid()),
                      decoration: InputDecoration(
                        prefixIcon: const Icon(Icons.person),
                        hintText: 'Username',
                        labelText: 'Username',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                      controller: username,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value) {
                        if (username.text.isEmpty) {
                          return 'Please enter your Username';
                        } else {
                          return null;
                        }
                      },
                    ),

                    const SizedBox(height: 16),
                    TextFormField(
                      onChanged: (value) => setState(() => valid = isValid()),
                      decoration: InputDecoration(
                        prefixIcon: const Icon(Icons.email),
                        hintText: 'Email',
                        labelText: 'Email',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                      controller: email,
                      keyboardType: TextInputType.emailAddress,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value) {
                        if (email.text.isEmpty) {
                          return 'Please enter your Email';
                        } else if (!EmailValidator.validate(email.text)) {
                          return 'Please enter a valid Email';
                        } else {
                          return null;
                        }
                      },
                    ),
                    const SizedBox(height: 16),
                    TextFormField(
                      onChanged: (value) => setState(() => valid = isValid()),
                      decoration: InputDecoration(
                        prefixIcon: const Icon(Icons.password),
                        hintText: 'Password',
                        labelText: 'Password',

                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                      obscureText: true,
                      controller: password,
                      keyboardType: TextInputType.text,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value) {
                        if (password.text.isEmpty) {
                          return 'Please enter your Password';
                        } else if(password.text.length <6){
                          return 'Password must be longer than 6';
                        } else {
                          return null;
                        }
                      },
                    ),
                    const SizedBox(height: 16),
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                          valid ? Theme.of(context).primaryColor : Colors.grey,
                        ),
                        foregroundColor: MaterialStateProperty.all(Colors.white),
                        shape: MaterialStateProperty.all(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                      ),

                      onPressed: valid ? () => register() : null,

                      child: registerIn
                          ? const SizedBox(
                              height: 25,
                              width: 25,
                              child: CircularProgressIndicator(color: Colors.white),
                      ) : Text(localizations.register),
                    ),
                  ],
                )
            ),
          );
        }),
      ),
    );
  }

  Future<void> register() async {
    setState(() => registerIn = true);
    ScaffoldMessengerState messenger = ScaffoldMessenger.of(context);

    try {
      if (formKey.currentState!.validate()) {
        formKey.currentState!.save();
      }
      if (formKey.currentState!.validate()) {
        var authService = AuthService.of(context);
        var navigator = Navigator.of(context);
        
        String? uid = await authService.register(
            context: context,
            email: email.text,
            password: password.text,
            displayName: username.text
        );

        if (uid != null) {
          await authService.login(email: email.text, password: password.text);
          navigator.pop();
        }
      }
    } catch (e) {
      setState(() => registerIn = false);
      messenger.showSnackBar(errorSnackBar(localizations.invalidCredentials));
    }
  }
  
  SnackBar errorSnackBar(String text) {
    return SnackBar(
        backgroundColor: Colors.red, 
        duration: const Duration(seconds: 3),
        content: Row(
            children: [
              const Icon(Icons.error, color: Colors.white),
              const SizedBox(width: 5),
              Text(
                  text,
                  style: const TextStyle(fontWeight: FontWeight.bold)
              )
        ])
    );
  }
  
  bool isValid() {
    return formKey.currentState?.validate() ?? false;
  }
}
