import 'package:cards/pages/auth/registration.dart';
import 'package:cards/services/auth_service.dart';
import 'package:cards/services/user_service.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool loggingIn = false;
  bool valid = false;
  bool emailTouched = false;
  bool passwordTouched = false;
  late AppLocalizations localizations;
  final FocusNode node = FocusNode();

  @override
  Widget build(BuildContext context) {
    localizations = AppLocalizations.of(context)!;

    return Scaffold(
      appBar: AppBar(
        title: Text(localizations.appName),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(15),
            child: Form(
                key: formKey,
                child: Column(
                  children: [
                      TextFormField(
                        onChanged: (value) => setState(() => valid = isValid()),
                        decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.email_outlined),
                          prefixIconColor: Theme.of(context).inputDecorationTheme.iconColor,
                          hintText: localizations.email,
                        ),
                        controller: emailController,
                        autovalidateMode: emailTouched
                            ? AutovalidateMode.onUserInteraction
                            : AutovalidateMode.disabled,
                        validator: (value) {
                          if (emailController.text.isEmpty || !EmailValidator.validate(emailController.text)) {
                            return localizations.enterValidEmail;
                          } else {
                            return null;
                          }
                        },
                        onTap: () {
                          setState(() {
                            emailTouched = true;
                          });
                        },

                        onFieldSubmitted: (s) => node.requestFocus(),
                    ),
                    const SizedBox(height: 10),
                    TextFormField(
                      focusNode: node,
                      onChanged: (value) => setState(() => valid = isValid()),
                      decoration: InputDecoration(
                        prefixIcon: const Icon(Icons.lock_open),
                        prefixIconColor: Theme.of(context).inputDecorationTheme.iconColor,
                        hintText: localizations.password,

                      ),
                      obscureText: true,
                      controller: passwordController,
                      autovalidateMode: passwordTouched
                          ? AutovalidateMode.onUserInteraction
                          : AutovalidateMode.disabled,
                      validator: (value) {
                        if (passwordController.text.isEmpty) {
                          return localizations.enterPassword;
                        } else if (passwordTouched && passwordController.text.length < 6) {
                          return localizations.tooSmallPasswordLength;
                        } else {
                          return null;
                        }
                      },
                      onTap: () {
                        setState(() {
                          passwordTouched = true;
                        });
                      },

                      onFieldSubmitted: (s) => login(),
                    ),
                  ],
                )),
          ),

          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: valid
                        ? MaterialStateProperty.all(
                            Theme.of(context).primaryColor)
                        : MaterialStateProperty.all(Colors.grey),
                    foregroundColor: MaterialStateProperty.all(Colors.white)),
                onPressed: valid
                    ? () => login()
                    : null,
                child: loggingIn
                    ? const SizedBox(
                        height: 25,
                        width: 25,
                        child: CircularProgressIndicator(color: Colors.white))
                    : Text(localizations.login)),
            TextButton(
                onPressed: () {
                  showRegistration(context);
                },
                child: Text(localizations.register))
          ])
        ],
      ),
    );
  }

  Future<void> showRegistration(BuildContext context) async {
    NavigatorState navigator = Navigator.of(context);

    await navigator.push(MaterialPageRoute(builder: (context) {
      return Consumer<UserService>(builder: (context, userService, child) {
        return Scaffold(
          appBar: AppBar(
            title: Text(localizations.createAccount),
          ),
          body: Registration(
              onSave: (user) async {},
              confirmText: localizations.signUp,
              user: null
          ),
        );
      });
    }));
  }

  Future<void> login() async {
    setState(() => loggingIn = true);
    ScaffoldMessengerState messenger = ScaffoldMessenger.of(context);

    try {
      if (formKey.currentState!.validate()) {
        formKey.currentState!.save();
      
        var authService = AuthService.of(context);
        var userService = UserService.of(context);

        await authService.login(
            email: emailController.text,
            password: passwordController.text
        );

        await userService.loadUserDetails();
      }
    } catch (e) {
      setState(() => loggingIn = false);

      messenger.showSnackBar(SnackBar(
          backgroundColor: Colors.red,
          content: Row(children: [
            const Icon(Icons.error, color: Colors.white),
            const SizedBox(width: 5),
            Text(localizations.invalidCredentials,
                style: const TextStyle(fontWeight: FontWeight.bold))
          ])));
    }
  }

  bool isValid() {
    return formKey.currentState?.validate() ?? false;
  }
}
