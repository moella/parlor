import 'package:cards/classes/game.dart';
import 'package:cards/pages/games/game_tile.dart';
import 'package:cards/services/game_service.dart';
import 'package:cards/utils.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Games extends StatefulWidget {
  const Games({super.key});

  @override
  State<Games> createState() => _GamesState();
}

class OrderFunction<T> {
  String label;
  int Function(T, T) orderFunction;

  OrderFunction({required this.label, required this.orderFunction});
}

class _GamesState extends State<Games> {
  int Function(Game, Game) orderFunction = (a, b) => a.name.compareTo(b.name);
  
  @override
  Widget build(BuildContext context) {
    AppLocalizations localizations = AppLocalizations.of(context)!;

    AppBar toolBar = AppBar(
      actions: [
        PopupMenuButton<OrderFunction<Game>>(
          itemBuilder: _popupMenuBuilder,
          icon: const Icon(Icons.sort),
          onSelected: (f) => setState(() => setState(() => orderFunction = f.orderFunction)),
        )
      ],
    );

    return Consumer<GameService>(
        builder: (context, gameService, child) {
          return futureBuilderOrLoading(future: gameService.games, builder: (games) {
            if (games != null && games.isNotEmpty) {

              var gamesCopy = List<Game>.of(games);
              gamesCopy.sort(orderFunction);

              return Column(
                children: [
                  toolBar,

                  Expanded(
                    child: RefreshIndicator(
                      onRefresh: () async => await gameService.loadGames(),
                      child: ListView.builder(
                        padding: const EdgeInsets.all(10),
                        itemCount: gamesCopy.length,
                        itemBuilder: (context, index) => GameTile(gamesCopy[index])
                      ),
                    ),
                  )
                ]
              );
            } else {
              return Center(child: Text(localizations.games_noGamesFound));
            }
          });
      },
    );
  }

  List<PopupMenuEntry<OrderFunction<Game>>> _popupMenuBuilder(BuildContext context) {
    AppLocalizations localizations = AppLocalizations.of(context)!;

    return [
      PopupMenuItem(
        value: OrderFunction(label: localizations.games_order_name_asc, orderFunction: (a, b) => a.name.compareTo(b.name)),
        child: Text(localizations.games_order_name_asc),
      ),

      PopupMenuItem(
        value: OrderFunction(label: localizations.games_order_name_desc, orderFunction: (a, b) => -a.name.compareTo(b.name)),
        child: Text(localizations.games_order_name_desc),
      ),
    ];
  }
}
