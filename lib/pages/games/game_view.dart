import 'package:cards/classes/game.dart';
import 'package:cards/services/user_service.dart';
import 'package:cards/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class GameView extends StatefulWidget {
  final Game game;

  const GameView(this.game, {super.key});

  @override
  State<GameView> createState() => _GameViewState();
}

class _GameViewState extends State<GameView> {
  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context)!;
    var userService = UserService.of(context);
    var game = widget.game;

    return Scaffold(
      appBar: AppBar(
        title: Text(game.name),
        actions: [
          IconButton(onPressed: () {}, icon: const Icon(Icons.play_arrow))
        ],
      ),

      body: Padding(
        padding: const EdgeInsets.all(10),
        child: ListView(
          children: [
            Card(child: Padding(
              padding: const EdgeInsets.all(15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(game.description, style: const TextStyle(fontSize: 16)),

                  futureBuilderOrLoading(future: userService.getUsernameFromUID(game.creator), builder: (username) {
                    return Text('${localizations.game_createdBy} $username', textAlign: TextAlign.right);
                  })
                ]
              ),
            ))
          ],
        ),
      )
    );
  }
}
