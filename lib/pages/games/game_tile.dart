import 'package:cards/classes/game.dart';
import 'package:cards/pages/games/game_view.dart';
import 'package:flutter/material.dart';

class GameTile extends StatefulWidget {
  final Game game;

  const GameTile(this.game, {super.key});

  @override
  State<GameTile> createState() => _GameTileState();
}

class _GameTileState extends State<GameTile> {
  @override
  Widget build(BuildContext context) {
    var game = widget.game;

    return Card(
      elevation: 5,
      child: ListTile(
        title: Text(game.name, style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
        subtitle: Text(game.description),
        onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => GameView(game))),
        trailing: IconButton(
          icon: const Icon(Icons.play_arrow),
          onPressed: () {},
        ),
      ),
    );
  }
}
