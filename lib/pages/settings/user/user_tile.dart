import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../services/auth_service.dart';
import 'user_editing_view.dart';

class UserTile extends StatelessWidget {
  const UserTile({super.key});

  @override
  Widget build(BuildContext context) {
    AppLocalizations localizations = AppLocalizations.of(context)!;
    var firebase = FirebaseAuth.instance;

    return Card(
      elevation: 4.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),

      margin: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
      child: ListTile(
          contentPadding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
          title: Text(
            firebase.currentUser?.displayName ?? localizations.userNotCouldNotBeLoaded,
            style: const TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18.0,
            ),
          ),

          subtitle: Text(
            FirebaseAuth.instance.currentUser?.email ?? 'no email',
            style: TextStyle(
              color: Colors.grey[600],
            )
          ),

        trailing: SizedBox(
          width: 100,
          child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            IconButton(
                onPressed: () => Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => const UserEditingView())),
                icon: const Icon(Icons.edit)
            ),

            IconButton(
                onPressed: () => AuthService.of(context).logout(),
                icon: const Icon(Icons.logout)
            )
        ]),
      ),
    ));
  }
}
