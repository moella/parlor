import 'package:cards/services/user_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class EmailEditingField extends StatefulWidget {
  const EmailEditingField({super.key});

  @override
  State<EmailEditingField> createState() => _EmailEditingFieldState();
}

class _EmailEditingFieldState extends State<EmailEditingField> {
  bool isLoading = false;

  TextEditingController emailController = TextEditingController();

  @override
  void initState() {
    super.initState();
    emailController.text = FirebaseAuth.instance.currentUser?.email ?? '';
  }

  @override
  Widget build(BuildContext context) {
    UserService userService = UserService.of(context);

    return Padding(
      padding: const EdgeInsets.only(top: 15),
      child: Row(
        children: [
          Flexible(
            child: TextFormField(
              controller: emailController,
              decoration: const InputDecoration(
                labelText: 'Email',
                hintText: 'Enter your email',
              ),
            ),
          ),

          if (!isLoading)
            IconButton(
                onPressed: () async {
                  setState(() => isLoading = true);
                  await userService.updateEmail(emailController.text);
                  setState(() => isLoading = false);
                },
                icon: const Icon(Icons.save)
            )
          else
            const Padding(
                  padding: EdgeInsets.all(5),
                  child: CircularProgressIndicator()
            )
        ],
      ),
    );
  }
}
