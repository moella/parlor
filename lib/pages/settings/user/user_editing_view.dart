import 'package:cards/classes/user.dart';
import 'package:cards/services/user_service.dart';
import 'package:cards/utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'email_editing_field.dart';
import 'username_editing_field.dart';

class UserEditingView extends StatefulWidget {
  const UserEditingView({super.key});

  @override
  State<UserEditingView> createState() => _UserEditingViewState();
}

class _UserEditingViewState extends State<UserEditingView> {
  UserDetails? userDetails;
  bool isLoading = true;

  TextEditingController usernameController = TextEditingController();
  TextEditingController emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var firebase = FirebaseAuth.instance;

    usernameController.text =  firebase.currentUser?.displayName ?? '';
    emailController.text = firebase.currentUser?.email ?? '';

    return Scaffold(
      appBar: AppBar(
        title: const Text('Edit profile'),
      ),

      body: const Padding(
          padding: EdgeInsets.all(10),
          child: Column(
              children: [
                UsernameEditingField(),
                EmailEditingField()
              ]
          )
      )
    );
  }
}
