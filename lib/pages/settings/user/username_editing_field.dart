import 'package:cards/classes/user.dart';
import 'package:cards/services/user_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

class UsernameEditingField extends StatefulWidget {
  const UsernameEditingField({super.key});

  @override
  State<UsernameEditingField> createState() => _UsernameEditingFieldState();
}

class _UsernameEditingFieldState extends State<UsernameEditingField> {
  bool isLoading = false;

  TextEditingController usernameController = TextEditingController();

  @override
  void initState() {
    super.initState();
    usernameController.text = FirebaseAuth.instance.currentUser?.displayName ?? '';
  }

  @override
  Widget build(BuildContext context) {
    UserService userService = UserService.of(context);

    return Padding(
      padding: const EdgeInsets.only(top: 15),
      child: Row(
        children: [
          Flexible(
            child: TextFormField(
              controller: usernameController,
              decoration: const InputDecoration(
                labelText: 'Username',
                hintText: 'Enter your username',
              ),
            ),
          ),

          if (!isLoading)
            IconButton(
                onPressed: () async {
                  setState(() => isLoading = true);
                  await userService.updateUsername(usernameController.text);
                  setState(() => isLoading = false);
                },
                icon: const Icon(Icons.save)
            )
          else
            const Padding(
                padding: EdgeInsets.all(5),
                child: CircularProgressIndicator()
            )
        ],
      ),
    );
  }
}
